#!/usr/bin/env ruby

class SvgGenerator
  DIGITS = [[%w[50 1 99 1]],
            [%w[50 50 99 50]],
            [%w[50 1 99 50]],
            [%w[50 50 99 1]],
            [%w[50 50 99 1], %w[50 1 99 1]],
            [%w[99 1 99 50]],
            [%w[99 1 99 50], %w[50 1 99 1]],
            [%w[99 1 99 50], %w[50 50 99 50]],
            [%w[99 1 99 50], %w[50 50 99 50], %w[50 1 99 1]]].freeze
  TENS = [[%w[1 1 50 1]],
          [%w[1 50 50 50]],
          [%w[1 50 50 1]],
          [%w[1 1 50 50]],
          [%w[1 1 50 50], %w[1 1 50 1]],
          [%w[1 1 1 50]],
          [%w[1 1 1 50], %w[1 1 50 1]],
          [%w[1 1 1 50], %w[1 50 50 50]],
          [%w[1 1 1 50], %w[1 1 50 1], %w[1 50 50 50]]].freeze
  HUNDREDS = [[%w[50 159 99 159]],
              [%w[50 110 99 110]],
              [%w[50 159 99 110]],
              [%w[50 110 99 159]],
              [%w[50 110 99 159], %w[50 159 99 159]],
              [%w[99 159 99 110]],
              [%w[99 159 99 110], %w[50 159 99 159]],
              [%w[99 159 99 110], %w[50 110 99 110]],
              [%w[99 159 99 110], %w[50 110 99 110], %w[50 159 99 159]]].freeze
  THOUSANDS = [[%w[1 159 50 159]],
               [%w[1 110 50 110]],
               [%w[1 110 50 159]],
               [%w[1 159 50 110]],
               [%w[1 159 50 110], %w[1 159 50 159]],
               [%w[1 159 1 110]],
               [%w[1 159 1 110], %w[1 159 50 159]],
               [%w[1 159 1 110], %w[1 110 50 110]],
               [%w[1 159 1 110], %w[1 159 50 159], %w[1 110 50 110]]].freeze

  def initialize
    @counter = 1
    @output_folder = 'glyphs/'
  end

  def call
    draw_digits
    draw_tens
    draw_hundreds
    draw_thousands
  end

  private

  def generate_svg(content)
    "<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"160\" width=\"100\" version=\"1.1\">#{content} /></svg>"
  end

  def draw_line(coordinates)
    "<line x1=\"#{coordinates[0]}\" y1=\"#{coordinates[1]}\" x2=\"#{coordinates[2]}\" y2=\"#{coordinates[3]}\" style=\"stroke:rgb(0,0,0);stroke-width:2\" />"
  end

  def vertical_line
    draw_line(%w[50 0 50 160])
  end

  def draw_number(coordinates_group)
    line = coordinates_group.map { |coordinates| draw_line(coordinates) }.join
    svg = generate_svg(vertical_line + line)
    File.open("#{@output_folder}#{@counter}.svg", 'w') { |file| file.write(svg) }
    @counter += 1
  end

  def draw_digits(additional_coordinates = [])
    DIGITS.map do |coordinates_group|
      draw_number(coordinates_group + additional_coordinates)
    end
  end

  def draw_tens(additional_coordinates = [])
    TENS.map do |ten|
      draw_number(additional_coordinates + ten)
      draw_digits(additional_coordinates + ten)
    end
  end

  def draw_hundreds(additional_coordinates = [])
    HUNDREDS.map do |hundred|
      draw_number(additional_coordinates + hundred)
      draw_digits(additional_coordinates + hundred)
      draw_tens(additional_coordinates + hundred)
    end
  end

  def draw_thousands
    THOUSANDS.map do |thousand|
      draw_number(thousand)
      draw_digits(thousand)
      draw_tens(thousand)
      draw_hundreds(thousand)
    end
  end
end

SvgGenerator.new.call
